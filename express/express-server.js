const express = require('express')
const cors = require('cors')

const users = require('./routes/users')

const PORT = 3002
const app = express()

app.use(cors({ origin: 'http://localhost:3000' }))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use('/users', users)

app.listen(PORT, () => {
  console.log(`Express server has been started on ${PORT} port...`)
})
