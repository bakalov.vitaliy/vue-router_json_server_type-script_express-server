const express = require('express')
const { body, validationResult } = require('express-validator')
const axios = require('axios')

const router = express.Router()

axios.defaults.baseURL = 'http://localhost:3001'

router.get('/', (req, res) => {
  axios
    .get('/users')
    .then(({ data }) => {
      res.send(data)
    })
    .catch(({ response }) => { handleResponseError(res, response) })
})

router.post(
  '/',
  body('name').notEmpty().isString(),
  body('surname').notEmpty().isString(),
  body('age').notEmpty().isInt(),
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    axios.post('/users', req.body)
      .then(({ data }) => res.send(data))
      .catch(({ response }) => { handleResponseError(res, response) })
  }
)

router.get('/:id', (req, res) => {
  axios.get('/users/' + req.params.id)
    .then(({ data }) => res.send(data))
    .catch(({ response }) => { handleResponseError(res, response) })
})

router.patch(
  '/:id',
  body('id').notEmpty().isInt(),
  body('name').notEmpty().isString(),
  body('surname').notEmpty().isString(),
  body('age').notEmpty().isInt(),
  (req, res) => {
    axios.patch('/users/' + req.params.id, req.body)
      .then(({ data }) => res.send(data))
      .catch(({ response }) => { handleResponseError(res, response) })
  }
)

router.delete('/:id', (req, res) => {
  axios.delete('/users/' + req.params.id)
    .then(({ data }) => res.send(data))
    .catch(({ response }) => { handleResponseError(res, response) })
})

function handleResponseError(res, response) {
  if (!response) {
    res.status(502).send(new Error('Нет связи с сервером'))
    return
  }

  res.status(response.status).send(new Error(response.statusText))
}

module.exports = router
