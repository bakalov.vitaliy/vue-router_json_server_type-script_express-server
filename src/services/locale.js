import Vue from 'vue'
import VueI18n from 'vue-i18n'
import ru from '@/locales/ru/index'
import en from '@/locales/en/index'

Vue.use(VueI18n)

const DEFAULT_LOCALE = 'ru'
const AVAILABLE_LOCALES = ['ru', 'en']

export default new VueI18n({
  locale: DEFAULT_LOCALE,
  fallbackLocale: DEFAULT_LOCALE,
  messages: {
    ru,
    en
  },
  availableLocales: AVAILABLE_LOCALES
})
